# Add here internal Software Heritage dependencies, one per line.
swh.core >= 0.3
swh.model >= 6.13.0
swh.auth
swh.web.client
